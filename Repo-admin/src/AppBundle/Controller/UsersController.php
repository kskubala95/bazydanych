<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\BuzzBundle\SensioBuzzBundle;

class UsersController extends Controller
{
	const API_URL = 'http://api.wypozyczalnia/';
	const API_USERS = 'users';
	const API_USERS_NEW = 'users/new';
	
	/**
	 * @Route("/users", name="users")
	 */
	public function indexAction(Request $request)
	{
		$buzz = $this->container->get('buzz');
		
		$headers = [];
		
		$responseJson = $buzz->get(self::API_URL . self::API_USERS, $headers);
		$users = json_decode($responseJson->getContent(), true);
		
		return $this->render('Users/users.html.twig', [
				'data' => [
						'users' => $users['data'],
						]
				]);
	}
	
	/**
	 * @Route("/users/new", name="users_new")
	 */
	public function usersNewAction(Request $request)
	{
		return $this->render('Users/users_new.html.twig', []);
	}
	
	/**
	 * @Route("/users/profile", name="users_profile")
	 */
	public function usersProfile(Request $request)
	{
		return $this->render('Users/users_profile.html.twig', []);
	}
	
	
	/**
	 * @Route("/users/new/post", name="users_new_post")
	 */
	public function usersNewPostAction(Request $request)
	{
		
		$buzz = $this->container->get('buzz');
		
		$firstname = $request->get('firstname');
		$lastname = $request->get('lastname');
		$email = $request->get('email');
		
		$headers = [];
		
		if(empty($firstname) || empty($lastname) || empty($email)) {
			return new JsonResponse(['error' => 'Missing data']);
		}
		
		$body = [
				'user' => [
						'firstname' => $firstname,
						'lastname'	=> $lastname,
						'email' => $email,
				],
		];
		
		$responseJson = $buzz->post(self::API_URL . self::API_USERS_NEW, $headers, json_encode($body));
		$response = json_decode($responseJson->getContent(), true);
		
		if(empty($response['error'])) {
			return new JsonResponse(['url' => $this->generateUrl('users')]);
		} else {
			return new JsonResponse($response);
		}
	}
}
