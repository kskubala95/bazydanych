<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ManualOrderController extends Controller
{
	const API_URL = 'http://api.wypozyczalnia/';
	const API_ORDERS = 'orders';
	const API_ORDERS_NEW = 'orders/new';
	
	/**
	 * @Route("/orders", name="orders")
	 */
	
	public function indexAction(Request $request)
	{
		$buzz = $this->container->get('buzz');
	
		$headers = [];
	
		$responseJson = $buzz->get(self::API_URL . self::API_ORDERS, $headers);
		$orders = json_decode($responseJson->getContent(), true);
	
		return $this->render('Orders/orders.html.twig', [
				'data' => [
						'orders' => $orders['data'],
				]
		]);
	}
	
	/**
	 * @Route("/orders/new", name="orders_new")
	 */
	public function ordersNewAction(Request $request)
	{
		return $this->render('Orders/orders_new.html.twig', []);
	}
	
	/**
	 * @Route("/orders/new/post", name="orders_new_post")
	 */
	public function ordersNewPostAction(Request $request)
	{
	
		$buzz = $this->container->get('buzz');
	/* w sumie wystarczy chyba tylko id uzytkownika lub jakis login bez reszty*/
		$firstname = $request->get('firstname');
		$lastname = $request->get('lastname');
		$driving_license = $request->get('driving_license'); // numer Prawa Jazdy
		$rental_office = $request->get('rental_office'); //obiekt w ktorym wypozyczono np. centrum, lotnisko, srodmiescie
		$begin = $request -> get('begin'); //poczatek wynajmu
		$end = $request -> get('end'); // koniec wynajmu
		$brand = $request -> get('brand');
		$model = $request -> get('model');
		$registrationNr= $reguest -> get('registration_nr'); // nr rejestracyjny
		$capacity = $request -> get('capacity'); // pojemnosc silnika
		$mileage = $request -> get('mileage'); //przebieg
		$accessories= $request ->get('accessories'); // wyposazenie samochodu
		$color = $request ->get('color'); // kolor pojazdu
		
		
		
		
		$headers = [];
	
		if(empty($firstname) || empty($lastname) || empty($RentalOffice)) {
			return new JsonResponse(['error' => 'Missing data']);
		}
	
		$body = [
				'order' => [
						'firstname' => $firstname,	'lastname'	=> $lastname,	'RentalOffice' => $RentalOffice, 'ID'=>$ID, 'DrivingLicense'=>$DrivingLicense,'Begin'=>$Begin,'End'=>$End,'Brand'=>$Brand,'Model'=>$Model,'RegistrationNr'=>$registrationNr,'Capacity'=>$Capacity,'Mileage'=>$Mileage,'Accessories'=>$Accessories,'Color'=>$Color,
				],
		];
	
		$responseJson = $buzz->post(self::API_URL . self::API_ORDERS_NEW, $headers, json_encode($body));
		$response = json_decode($responseJson->getContent(), true);
	
		if(empty($response['error'])) {
			return new JsonResponse(['url' => $this->generateUrl('orders')]);
		} else {
			return new JsonResponse($response);
		}
	}
	}
	