$(document).ready(function(){
	$('#btn-new-user').on('click', function(e) {
		e.preventDefault();
		$('#error').html('');
		if($('#firstname').val() == "" || $('#lastname').val() == "" || $('#email').val() == "") {
			$('#error').html('Wypelnij wszystko!!!');
		} else {
			var data = {
					firstname: $('#firstname').val(),
					lastname: $('#lastname').val(),
					email: $('#email').val(),
			};
			$.post('/users/new/post', data, function(response){
				if(response.url) {
					window.location.href = response.url;
				} else {
					$('#error').html(response.error);
				}
			});
		}
	});
	$('#btn-new-order').on('click', function(e) {
		e.preventDefault();
		$('#error').html('');
		if($('#firstname').val() == "" || $('#lastname').val() == "" || $('#driving_license').val() == ""|| $('#rental_office').val() == ""|| $('#begin').val() == ""|| $('#end').val() == ""|| $('#brand').val() == ""|| $('#model').val() == ""|| $('#registration_nr').val() == ""|| $('#capacity').val() == ""|| $('#mileage').val() == ""|| $('#accessories').val() == ""|| $('#color').val() == "") {
			$('#error').html('Wypelnij wszystko!!!');
		} else {
			var data = {
					firstname: $('#firstname').val(),
					lastname: $('#lastname').val(),
					driving_license: $('#driving_license').val(),
					rental_office: $('#rental_office').val(),
					begin: $('#begin').val(),
					end: $('#end').val(),
					brand: $('#brand').val(),
					model: $('#model').val(),
					registration_nr: $('#registration_nr').val(),
					capacity: $('#capacity').val(),
					mileage: $('#mileage').val(),
					accessories: $('#accessories').val(),
					color: $('#color').val(),
			};
			$.post('/orders/new', data, function(response){
				if(response.url) {
					window.location.href = response.url;
				} else {
					$('#error').html(response.error);
				}
			});
		}
	});
});
